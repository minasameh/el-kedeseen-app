package com.elkedeseen.beloghos;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.widget.Toast;

import com.elkedeseen.beloghos.utils.Utils;
import com.google.android.youtube.player.YouTubePlayer;

public class Main extends AppCompatActivity {

    private static Fragment times = new Times();
    private static Fragment streaming = new Streaming();
    private static Fragment about = new About();

    public YouTubePlayer youTubePlayer;
    public boolean isYouTubePlayerFullScreen;


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment selectedFragment = null;
            switch (item.getItemId()) {
                case R.id.times:
                    selectedFragment = times;
                    break;
                case R.id.streaming:
                    selectedFragment = streaming;
                    break;
                case R.id.about:
                    selectedFragment = about;
                    break;
//                case R.id.martyrus:
//                    selectedFragment = new Martyrus();
//                    break;
//                case R.id.priests:
//                    selectedFragment = new Priests();
//                    break;
            }
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.content, selectedFragment);
            transaction.commit();
            return true;
//            switch (item.getItemId()) {
//                case R.id.times:
//                    mTextMessage.setText(R.string.times);
//                    return true;
//                case R.id.streaming:
//                    mTextMessage.setText(R.string.streaming);
//                    return true;
//                case R.id.martyrus:
//                    mTextMessage.setText(R.string.martyrus);
//                    return true;
//                case R.id.priests:
//                    mTextMessage.setText(R.string.priests);
//                    return true;
//            }
//            return false;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content, new Streaming());
        transaction.commit();

        checkVersion();
    }

    private void checkVersion() {
        int version = Utils.getIntPref(getString(R.string.last_version), getApplicationContext());
        if(version > BuildConfig.VERSION_CODE) {
            AlertDialog.Builder builder;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Alert);
            } else {
                builder = new AlertDialog.Builder(this);
            }
            builder.setTitle("تحديث")
                    .setMessage("يوجد نسخه جديده من التطبيق هل تريد التحديث الان ؟")
                    .setPositiveButton("بالتأكيد !", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                            try {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                            } catch (android.content.ActivityNotFoundException anfe) {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                            }
                        }
                    })
                    .setNegativeButton("فى وقت لاحق", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // do nothing
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }
    }

    @Override
    public void onBackPressed() {
        if (youTubePlayer != null && isYouTubePlayerFullScreen){
            youTubePlayer.setFullscreen(false);
        } else {
            super.onBackPressed();
        }
    }

}
