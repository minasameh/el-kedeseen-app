package com.elkedeseen.beloghos.utils;

import com.elkedeseen.beloghos.BuildConfig;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;

/**
 * Created by mina on 07/12/17.
 */

public class Network {

    private static OkHttpClient client;

    private static OkHttpClient getClient() {
        if(client == null)
             client = new OkHttpClient();
        return client;
    }

    public static void getRadioList(Callback callback) {
        Request request = new Request.Builder()
                .url("http://elkedeseen.org/api/radio/read.php")
                .build();

        getClient().newCall(request).enqueue(callback);
    }

    public static void getTimesList(Callback callback) {
        Request request = new Request.Builder()
                .url("http://elkedeseen.org/api/times/read.php")
//                .url("http://elkedeseen.org/api/test.php")
                .build();

        getClient().newCall(request).enqueue(callback);
    }

    public static void getVersion(Callback callback) {
        Request request = new Request.Builder()
                .url("http://elkedeseen.org/api/app/version.php")
                .build();

        getClient().newCall(request).enqueue(callback);
    }



}
