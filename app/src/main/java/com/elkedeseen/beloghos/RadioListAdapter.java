package com.elkedeseen.beloghos;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.elkedeseen.beloghos.model.DBHelper;

/**
 * Created by mina on 07/12/17.
 */

class RadioListAdapter extends BaseAdapter {

    String[][] items;
    Context context;

    public RadioListAdapter(Context context) {
        this.context = context;
        items = DBHelper.getInstance(context).getTodayRadio();

    }

    @Override
    public int getCount() {
        return items == null ? 0: items.length - 1;
    }

    @Override
    public String[] getItem(int position) {
        return items[position+1];
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        if(view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.radio_list_item, parent, false);
        }

        TextView title = (TextView) view.findViewById(R.id.title);

        TextView text = (TextView) view.findViewById(R.id.text);

        String[] item = getItem(position);

        title.setText(item[1]);
        text.setText(item[0]);

        return view;
    }
}
