package com.elkedeseen.beloghos;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.elkedeseen.beloghos.utils.Utils;

/**
 * Created by mina on 02/12/17.
 */

public class PlaceHolderFragment extends Fragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";

    public PlaceHolderFragment() {

    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static PlaceHolderFragment newInstance(int sectionNumber) {
        PlaceHolderFragment fragment = new PlaceHolderFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView;
        switch(getArguments().getInt(ARG_SECTION_NUMBER)){
            case 1:
                rootView = inflater.inflate(R.layout.tutorial1, container, false);
                break;
            case 2:
                rootView = inflater.inflate(R.layout.tutorial2, container, false);
                break;
            case 3:
                rootView = inflater.inflate(R.layout.tutorial3, container, false);
                break;
            case 4:
                rootView = inflater.inflate(R.layout.tutorial4, container, false);
                Button done = (Button) rootView.findViewById(R.id.skip);
                done.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Utils.putPref(getString(R.string.not_first_time), 1, getActivity().getApplicationContext());
                        Intent main = new Intent(getActivity(), Main.class);
                        main.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(main);
                    }
                });
                break;
            default:
                rootView = inflater.inflate(R.layout.tutorial1, container, false);
        }

        return rootView;
    }
}