package com.elkedeseen.beloghos;

import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.Toast;

import com.elkedeseen.beloghos.model.DBHelper;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerFragment;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;
import com.google.android.youtube.player.YouTubePlayerView;
import com.squareup.picasso.Picasso;


public class Streaming extends Fragment {

    YoutubeFragment fragment;
    public static ImageView playPause;
    RadioListAdapter adapter;

    public Streaming() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        CheckIfServiceIsRunning();

        fragment = new YoutubeFragment();
        FragmentManager manager = getFragmentManager();
        manager.beginTransaction()
                .replace(R.id.YotubePlayer, fragment)
                .commit();

        IntentFilter filter = new IntentFilter();
        filter.addAction("WOW");
        filter.addCategory(Intent.CATEGORY_DEFAULT);
        getActivity().registerReceiver(br, filter);

    }

    BroadcastReceiver br = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if("play".equals(intent.getStringExtra("type"))) {
                if (playPause != null) {
                    if (intent.getBooleanExtra("isPlaying", false)) {
                        Picasso.with(context).load(R.drawable.stop).into(playPause);
//                    playPause.setBackgroundResource(R.drawable.stop);
                    } else {
                        Picasso.with(context).load(R.drawable.recordicon).into(playPause);
//                    playPause.setBackgroundResource(R.drawable.recordicon);
                    }
                }
            }
            else if("ready".equals(intent.getStringExtra("type"))) {
                if(adapter != null) {
                    adapter.notifyDataSetChanged();
                }
            }
        }
    };

    @Override
    public void onPause() {
        super.onPause();
        try {
            getActivity().unregisterReceiver(br);
        } catch (Exception e ) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.streaming, container, false);

        playPause = (ImageView) view.findViewById(R.id.playPause);

        if(audioService.isPlaying){
            Picasso.with(getActivity()).load(R.drawable.stop).into(playPause);
//                    playPause.setBackgroundResource(R.drawable.stop);
        } else {
            Picasso.with(getActivity()).load(R.drawable.recordicon).into(playPause);
//                    playPause.setBackgroundResource(R.drawable.recordicon);
        }

//        playPause.setImageResource(R.drawable.recordicon);
//        CheckIfServiceIsRunning();

        playPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                CheckIfServiceIsRunning();
                if(audioService.isPlaying){
//                    doUnbindService();
                    Picasso.with(getActivity()).load(R.drawable.recordicon).into(playPause);
//                    playPause.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.recordicon));
//                    getActivity().stopService(new Intent(getActivity(), audioService.class));
//                    playPause.setBackgroundResource(R.drawable.recordicon);
                } else {
                    Picasso.with(getActivity()).load(R.drawable.stop).into(playPause);
//                    playPause.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.stop));
                }
                Intent playPause = new Intent(getActivity(), audioService.class);
                playPause.setAction("do");
                getActivity().startService(playPause);
            }
        });



        view.findViewById(R.id.container).scrollTo(100, 0);
        ScrollView sv = (ScrollView) view.findViewById(R.id.container);
        ListView lv = (ListView) view.findViewById(R.id.radio_list);
        sv.setEnabled(false);
        lv.setFocusable(false);

        adapter = new RadioListAdapter(getActivity());
        lv.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        sv.setEnabled(true);
        return view;
    }



//    Messenger mService = null;
//    boolean mIsBound;
//    final Messenger mMessenger = new Messenger(new Streaming.IncomingHandler());
//
//    class IncomingHandler extends Handler {
//        @Override
//        public void handleMessage(Message msg) {
//            if(playPause != null) {
//                if (audioService.isRunning()) {
//                    playPause.setBackgroundResource(R.drawable.stop);
//                } else {
//                    playPause.setBackgroundResource(R.drawable.recordicon);
//                }
//            }
////            switch (msg.what) {
////                case audioService.MSG_SET_INT_VALUE:
////                    textIntValue.setText("Int Message: " + msg.arg1);
////                    break;
////                case MyService.MSG_SET_STRING_VALUE:
////                    String str1 = msg.getData().getString("str1");
////                    textStrValue.setText("Str Message: " + str1);
////                    break;
////                default:
////                    super.handleMessage(msg);
////            }
//        }
//    }

//    private ServiceConnection mConnection = new ServiceConnection() {
//        public void onServiceConnected(ComponentName className, IBinder service) {
//            mService = new Messenger(service);
//            try {
//                Message msg = Message.obtain(null, audioService.MSG_REGISTER_CLIENT);
//                msg.replyTo = mMessenger;
//                mService.send(msg);
//            }
//            catch (RemoteException e) {
//                // In this case the service has crashed before we could even do anything with it
//            }
//        }
//
//        public void onServiceDisconnected(ComponentName className) {
//            // This is called when the connection with the service has been unexpectedly disconnected - process crashed.
//            mService = null;
//        }
//    };

//    private void CheckIfServiceIsRunning() {
//        //If the service is running when the activity starts, we want to automatically bind to it.
//        if (audioService.isRunning()) {
//            doBindService();
//        }
//    }


//    void doBindService() {
//        getActivity().bindService(new Intent(getActivity(), audioService.class), mConnection, Context.BIND_AUTO_CREATE);
//        mIsBound = true;
//    }
//
//    void doUnbindService() {
//        if (mIsBound) {
//            // If we have received the service, and hence registered with it, then now is the time to unregister.
//            if (mService != null) {
//                try {
//                    Message msg = Message.obtain(null, audioService.MSG_UNREGISTER_CLIENT);
//                    msg.replyTo = mMessenger;
//                    mService.send(msg);
//                }
//                catch (RemoteException e) {
//                    // There is nothing special we need to do if the service has crashed.
//                }
//            }
//            // Detach our existing connection.
//            getActivity().unbindService(mConnection);
//            mIsBound = false;
//        }
//    }

//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//        try {
//            //doUnbindService();
//        }
//        catch (Throwable t) {
//            Log.e("MainActivity", "Failed to unbind from the service", t);
//        }
//    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if(fragment != null)
            fragment.pausePlayer();
    }

}
