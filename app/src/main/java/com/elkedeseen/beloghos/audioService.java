package com.elkedeseen.beloghos;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.elkedeseen.beloghos.R;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by mina on 29/11/17.
 */

public class audioService extends Service implements MediaPlayer.OnPreparedListener {
//    public static final int MSG_REGISTER_CLIENT = 1;
//    public static final int MSG_UNREGISTER_CLIENT = 2;

    private NotificationManager nm;

//    ArrayList<Messenger> mClients = new ArrayList<Messenger>(); // Keeps track of all current registered clients.
//    static final int SERVICE = 2;
//    static final int MSG_PLAY = 2;
//    static final int MSG_PAUSE = 3;
//    final Messenger mMessenger = new Messenger(new IncomingHandler()); // Target we publish for clients to send messages to IncomingHandler.
    Notification.Builder mBuilder;
    MediaPlayer mMediaPlayer;

    public static boolean isPlaying = false;


//    @Override
//    public IBinder onBind(Intent intent) {
//        return mMessenger.getBinder();
//    }


//    class IncomingHandler extends Handler { // Handler of incoming messages from clients.
//        @Override
//        public void handleMessage(Message msg) {
//
//            switch (msg.what) {
//                case MSG_REGISTER_CLIENT:
//                    mClients.add(msg.replyTo);
//                    break;
//                case MSG_UNREGISTER_CLIENT:
//                    mClients.remove(msg.replyTo);
//                    break;
//                default:
//                    super.handleMessage(msg);
//            }
//        }
//    }

//    private void sendMessageToUI(int value) {
//        for (int i=mClients.size()-1; i>=0; i--) {
//            try {
//                // Send data as an Integer
//                mClients.get(i).send(Message.obtain(null, SERVICE, value, 0));
//            }
//            catch (RemoteException e) {
//                // The client is dead. Remove it from the list; we are going through the list from back to front so this is safe to do inside the loop.
//                mClients.remove(i);
//            }
//        }
//    }

//    public static boolean isRunning()
//    {
//        return isRunning;
//    }
//
//    public static boolean isStreammingRunning()
//    {
//        return isRunning && mMediaPlayer != null && mMediaPlayer.isPlaying();
//    }

    @Override
    public void onCreate() {
        super.onCreate();

        mMediaPlayer = new MediaPlayer();
        mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mMediaPlayer.setOnPreparedListener(this);



//        try {
//            mMediaPlayer.setDataSource(this, Uri.parse("http://88.99.36.93:8000/"));
////            mMediaPlayer.prepare();
//        } catch (Exception e) {
//            Toast.makeText(this, "error", Toast.LENGTH_SHORT).show();
//        }
        showNotification();

        Log.d("My", "la da kda estehbal");

//        playPauseButtonAction();
//        isRunning = true;
    }

    private void playPauseAction() {
        if(mMediaPlayer!=null && isPlaying){
            stop();
        } else {
            start();
        }
    }

    private void stop() {
        Log.d("My", "fy 7ad hna");
        mMediaPlayer.stop();
//        mMediaPlayer.release();
        isPlaying = false;

//        try {
//            mMediaPlayer.prepare();
//        } catch (Exception e) {
//            Toast.makeText(this, "can't play right now", Toast.LENGTH_SHORT).show();
//        }

//        mMediaPlayer.release();
//        mMediaPlayer = null;
//        isPlaying = false;
    }

    private void start() {
        mMediaPlayer.reset();
        try {
            mMediaPlayer.setDataSource(this, Uri.parse("http://88.99.36.93:8000/"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        mMediaPlayer.prepareAsync(); // prepare async to not block main thread
        isPlaying = true;
    }

    private void showNotification() {
        Log.d("My", "la b2a");
        nm = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
        // In this sample, we'll use the same text for the ticker and the expanded notification
        CharSequence text = getText(R.string.service_started);
        // The PendingIntent to launch our activity if the user selects this notification
        Intent i = new Intent(this, Main.class);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,i, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent playPause = new Intent(this, audioService.class);
        playPause.setAction("do");
        PendingIntent piPlayPause = PendingIntent.getService(this, 0, playPause, 0);

        Intent quit = new Intent(this, audioService.class);
        quit.setAction("quit");
        PendingIntent piQuit = PendingIntent.getService(this, 0, quit, 0);

        int id = mMediaPlayer!=null && isPlaying ? R.drawable.pause: R.drawable.play;

        String playPauseText =  mMediaPlayer!=null && isPlaying ? "ايقاف" : "تشغيل";

        mBuilder = new Notification.Builder(this)
                .setContentTitle(getString(R.string.Radio_text))
                .setContentIntent(contentIntent)
                .setSmallIcon(R.drawable.beloghos)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.beloghos))
                .setStyle(new Notification.BigTextStyle())
                .addAction (id,
                        playPauseText, piPlayPause)
                .addAction (R.drawable.close,
                        "خروج", piQuit);

        Notification n = mBuilder.build();
        n.flags |= Notification.FLAG_NO_CLEAR | Notification.FLAG_ONGOING_EVENT;
        startForeground(1, n);
//        nm.notify(R.string.service_started, n);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
//        Log.i("MyService", "Received start id " + startId + ": " + intent);
        Log.d("My", ""+intent);
        if(intent!=null){
            Log.d("My", ""+intent.getAction());
            if ("do".equals(intent.getAction())) {
                playPauseButtonAction();
            }

            if ("quit".equals(intent.getAction())) {
                quit();
            }
//            String s = "" + intent.getAction();
            sendState();
        }
        return START_STICKY; // run until explicitly stopped.
    }

    private void sendState() {
        Log.d("my", isPlaying+"");
        Intent broadcast = new Intent();
        broadcast.putExtra("isPlaying", isPlaying);
        broadcast.setAction("WOW");
        broadcast.addCategory(Intent.CATEGORY_DEFAULT);
        sendBroadcast(broadcast);
    }

    private void playPauseButtonAction() {
        playPauseAction();
        showNotification();
    }

    private void quit() {
        stopSelf();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("My", "bla7 el sham");
        isPlaying = false;
        sendState();
        nm.cancel(R.string.service_started); // Cancel the persistent notification.
        Log.i("MyService", "Service Stopped.");
        if(mMediaPlayer!=null)
            mMediaPlayer.release();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
        mediaPlayer.start();
        isPlaying = true;
    }
}
