package com.elkedeseen.beloghos;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.elkedeseen.beloghos.model.DBHelper;
import com.elkedeseen.beloghos.model.TimeItem;

import java.sql.Time;

/**
 * Created by mina on 08/12/17.
 */

public class TimeListAdabter extends BaseAdapter {

    TimeItem[] items;
    Context context;

    public TimeListAdabter(Context context) {
        this.context = context;
        items = DBHelper.getInstance(context).getTimes();
    }

    @Override
    public int getCount() {
        return items == null? 0: items.length;
    }

    @Override
    public TimeItem getItem(int i) {
        return items[i];
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        if(view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.time_item, viewGroup, false);
        }

        TextView title = (TextView) view.findViewById(R.id.title);

        TextView text = (TextView) view.findViewById(R.id.text);

        TimeItem item = getItem(i);

        title.setText(item.title);
        text.setText(item.body);

//        switch(i){
//            case 0:
//                title.setText("قداسات");
//                text.setText("يقام قداس كل يوم 7-9 ص\nيوم الاربع يقام قداس اضافى 1-3 ظ");
//                break;
//            case 1:
//                title.setText("عشيات");
//                String q = "تقام العشيات كل يوم 7-9 م طبقا للجدول التالى";
//                String w = "2/12/2017 - القس انطونيوس فهمى";
//                String e = "3/12/2017 - القس مكاريوس وهيب";
//                String r = "4/12/2017 - القس آبرام أميل";
//                String t = "5/12/2017 - نيافة الأنبا داود";
//                String y = "6/12/2017 - نيافة الأنبا بافلى";
//                String u = "7/12/2017 - نيافة الأنبا أبانوب";
//                text.setText(q+"\n"+w+"\n"+e+"\n"+r+"\n"+t+"\n"+y+"\n"+u);
//                break;
//            case 2:
//                title.setText("تسبحة");
//                text.setText("تقام تسبحة نصف الليل يوم السبت من كل اسبوع 9-11 م");
//                break;
//        }


        return view;
    }
}
