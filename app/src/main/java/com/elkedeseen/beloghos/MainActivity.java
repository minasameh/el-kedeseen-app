package com.elkedeseen.beloghos;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

public class MainActivity extends YouTubeBaseActivity {
    static String key = "AIzaSyC4nC7NFgX_MHbyWq7Rjd5Ml95egVfPP9s";

    String d = "88.99.36.93:8000";

    YouTubePlayerView youTubePlayerView;
    Button button;
    YouTubePlayer.OnInitializedListener onInitializedListener;

    Messenger mService = null;
    boolean mIsBound;
    final Messenger mMessenger = new Messenger(new IncomingHandler());

    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
//            switch (msg.what) {
//                case audioService.MSG_SET_INT_VALUE:
//                    textIntValue.setText("Int Message: " + msg.arg1);
//                    break;
//                case MyService.MSG_SET_STRING_VALUE:
//                    String str1 = msg.getData().getString("str1");
//                    textStrValue.setText("Str Message: " + str1);
//                    break;
//                default:
//                    super.handleMessage(msg);
//            }
        }
    }

    private ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            mService = new Messenger(service);
//            try {
//                Message msg = Message.obtain(null, audioService.MSG_REGISTER_CLIENT);
//                msg.replyTo = mMessenger;
//                mService.send(msg);
//            }
//            catch (RemoteException e) {
//                // In this case the service has crashed before we could even do anything with it
//            }
        }

        public void onServiceDisconnected(ComponentName className) {
            // This is called when the connection with the service has been unexpectedly disconnected - process crashed.
            mService = null;
        }
    };

//    private void CheckIfServiceIsRunning() {
//        //If the service is running when the activity starts, we want to automatically bind to it.
//        if (audioService.isRunning()) {
//            doBindService();
//        }
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        youTubePlayerView = (YouTubePlayerView) findViewById(R.id.YotubePlayer);

        youTubePlayerView.initialize(key, new YouTubePlayer.OnInitializedListener(){

            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {

//                youTubePlayer.loadVideo("n-qccYxLkMM");
                youTubePlayer.cueVideo("n-qccYxLkMM");

//
//                youTubePlayer.pause();

            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

            }

        });

//        Button btnStart = (Button) findViewById(R.id.start);
//        Button btnStop = (Button) findViewById(R.id.stop);

//        btnStart.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startService(new Intent(MainActivity.this, audioService.class));
//            }
//        });
//
//        btnStop.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                doUnbindService();
//                stopService(new Intent(MainActivity.this, audioService.class));
//            }
//        });
//        CheckIfServiceIsRunning();

    }

//    void doBindService() {
//        bindService(new Intent(this, audioService.class), mConnection, Context.BIND_AUTO_CREATE);
//        mIsBound = true;
//    }
//
//    void doUnbindService() {
//        if (mIsBound) {
//            // If we have received the service, and hence registered with it, then now is the time to unregister.
//            if (mService != null) {
//                try {
//                    Message msg = Message.obtain(null, audioService.MSG_UNREGISTER_CLIENT);
//                    msg.replyTo = mMessenger;
//                    mService.send(msg);
//                }
//                catch (RemoteException e) {
//                    // There is nothing special we need to do if the service has crashed.
//                }
//            }
//            // Detach our existing connection.
//            unbindService(mConnection);
//            mIsBound = false;
//        }
//    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        try {
//            doUnbindService();
//        }
//        catch (Throwable t) {
//            Log.e("MainActivity", "Failed to unbind from the service", t);
//        }
    }
}