package com.elkedeseen.beloghos;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by mina on 09/12/17.
 */

public class About extends Fragment {

    public About() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.about, container, false);

        view.findViewById(R.id.fbicon).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(newFacebookIntent(getContext().getPackageManager(), "https://www.facebook.com/ElkedeseenChurchOfficialPage"));
            }
        });

        view.findViewById(R.id.Website).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://elkedeseen.org/church/"));
                startActivity(browserIntent);
            }
        });

        view.findViewById(R.id.Youtube).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                watchYoutubeVideo();
            }
        });

        view.findViewById(R.id.SoundCloud).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                soundCloudIntent();
            }
        });

        view.findViewById(R.id.Phone).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:035548400"));
                startActivity(intent);
            }
        });

        return view;
    }

    public Intent newFacebookIntent(PackageManager pm, String url) {
        Uri uri = Uri.parse(url);
        try {
            ApplicationInfo applicationInfo = pm.getApplicationInfo("com.facebook.katana", 0);
            if (applicationInfo.enabled) {
                // http://stackoverflow.com/a/24547437/1048340
                uri = Uri.parse("fb://page/141367489358644");

            }
        } catch (PackageManager.NameNotFoundException ignored) {
        }
        return new Intent(Intent.ACTION_VIEW, uri);
    }

    public void watchYoutubeVideo() {
//        Intent appIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:n-qccYxLkMM"));
        Intent webIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("https://www.youtube.com/channel/UCzD8zUXN27dEGKE1MAYm4DA"));
//        try {
//            startActivity(appIntent);
//        } catch (ActivityNotFoundException ex) {
        startActivity(webIntent);
//        }
    }

    public void soundCloudIntent() {
        Intent appIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("soundcloud://users:189487397"));
        Intent webIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("https://soundcloud.com/beloghosradio"));
        try {
            startActivity(appIntent);
        } catch (ActivityNotFoundException ex) {
            startActivity(webIntent);
        }
    }
}
