package com.elkedeseen.beloghos;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateUtils;
import android.widget.ImageView;

import com.elkedeseen.beloghos.model.DBHelper;
import com.elkedeseen.beloghos.model.Res;
import com.elkedeseen.beloghos.model.Res2;
import com.elkedeseen.beloghos.model.Res3;
import com.elkedeseen.beloghos.utils.Network;
import com.elkedeseen.beloghos.utils.Utils;
import com.google.gson.Gson;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Tutorial extends AppCompatActivity {

    private SectionsPagerAdapter mSectionsPagerAdapter;

    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        reset();
//        DBHelper.getInstance(this).reset();

        startService(new Intent(this, audioService.class));
        checkUpdates();
        checkPrefs();
        checkVersion();
    }

    private void checkVersion() {
        Network.getVersion(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                String s;
            }
            @Override
            public void onResponse(Response response) throws IOException {
                Gson gson = new Gson();
                String text = response.body().string();
                Res3 item = gson.fromJson(text, Res3.class);
//                SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
//                SharedPreferences.Editor editor = sharedPref.edit();
                int version = item.records.version;
//                editor.putInt(getString(R.string.last_version), version);
//                editor.apply();
                Utils.putPref(getString(R.string.last_version), version, getApplicationContext());
            }
        });
    }

    private void checkPrefs() {
        if(Utils.getIntPref(getString(R.string.not_first_time), getApplicationContext()) == 1) {
            setContentView(R.layout.tutorial1);
            ((ImageView) findViewById(R.id.page1)).setAlpha(0);
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    gotoMain();
                }
            }, 3000);
        } else {
            setContentView(R.layout.tutorial);

            // Create the adapter that will return a fragment for each of the three
            // primary sections of the activity.
            mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

            // Set up the ViewPager with the sections adapter.
            mViewPager = (ViewPager) findViewById(R.id.container);
            mViewPager.setAdapter(mSectionsPagerAdapter);
        }
    }

    void reset() {
        Utils.putPref(getString(R.string.last_update), "", getApplicationContext());
       //        DBHelper.getInstance(this).reset();

    }

    private void gotoMain() {
        Intent main = new Intent(Tutorial.this, Main.class);
        main.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(main);
    }

    private void checkUpdates() {
        String lastUpdate = Utils.getStringPref(getString(R.string.last_update), getApplicationContext());

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date d = null;
        try {
            d = sdf.parse(lastUpdate);
        } catch (ParseException ex) {
        }


        if(d == null || !DateUtils.isToday(d.getTime())){
            Utils.putPref(getString(R.string.last_update), sdf.format(new Date()), getApplicationContext());
            Network.getRadioList(new Callback() {
                @Override
                public void onFailure(Request request, IOException e) {
                    String s;
                }
                @Override
                public void onResponse(Response response) throws IOException {
                    Gson gson = new Gson();
                    try {
                        String text = response.body().string();
                        Res item = gson.fromJson(text, Res.class);
                        DBHelper.getInstance(Tutorial.this).insertRadioItems(item.records);

                        //Todo:send broadcast to reload list

                    } catch (Exception e){
                    }
                }
            });
        }
        Network.getTimesList(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                String s;
            }
            @Override
            public void onResponse(Response response) throws IOException {
                Gson gson = new Gson();
                try {
                    String text = response.body().string();
                    Res2 item = gson.fromJson(text, Res2.class);
                    DBHelper.getInstance(Tutorial.this).insertTimes(item.records);
                } catch (Exception e){
                    String s;
                }
            }
        });
    }

    /**
     * A placeholder fragment containing a simple view.
     */


    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceHolderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return "SECTION 1";
        }
    }
}
