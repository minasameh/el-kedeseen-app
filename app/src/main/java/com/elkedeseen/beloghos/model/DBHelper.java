package com.elkedeseen.beloghos.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by mina on 07/12/17.
 */

public class DBHelper extends SQLiteOpenHelper {

    private static DBHelper mDBHelper;

    private static final String DATABASE_NAME = "db.db";
    private static final String RADIO_TABLE = "radio";
    private static final String TIMES_TABLE = "time";


    public static DBHelper getInstance(Context context) {
        if(mDBHelper == null)
            mDBHelper = new DBHelper(context);
        return mDBHelper;
    }


    private DBHelper(Context context) {
        super(context, DATABASE_NAME , null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table IF NOT EXISTS " + RADIO_TABLE + " (id integer primary key, day text, data text)");
        db.execSQL("create table IF NOT EXISTS " + TIMES_TABLE + " (id integer primary key, title text, body text)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists " + RADIO_TABLE );
        db.execSQL("drop table if exists " + TIMES_TABLE );
        onCreate(db);
    }

    public void reset() {
        SQLiteDatabase db = getWritableDatabase();
        onUpgrade(db, 0, 0);
    }

    public String[][] getTodayRadio() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        SQLiteDatabase db = this.getReadableDatabase();
        String today = sdf.format(new Date());
//        Cursor c = db.rawQuery("SELECT data FROM " + RADIO_TABLE + " WHERE day='"+sdf.format(new Date())+"'", null);
        Cursor c = db.rawQuery("SELECT data FROM " + RADIO_TABLE+" WHERE day='"+today+"'", null);
        c.moveToFirst();
        if(c.getCount() == 1) {
            String data = c.getString(0);
            try{
                Gson gson = new Gson();
                return gson.fromJson(data, String[][].class);
            } catch(Exception e) {
                return null;
            } finally {
                c.close();
            }
        } else {
            c.close();
            return null;
        }
    }

    public boolean isRadioExists(String day) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery("SELECT COUNT(*) FROM " + RADIO_TABLE + " WHERE day='"+day+"'", null);
        c.moveToFirst();
        return c.getInt(0) > 0;
    }

    public void insertRadioItems(RadioItem[] items) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        SQLiteDatabase db = this.getWritableDatabase();
        for(RadioItem item : items) {
            String date;
            try{
                date = sdf.format(sdf.parse(item.day));
            } catch (Exception e) {
                continue;
            }
            if(!isRadioExists(date)) {
                ContentValues contentValues = new ContentValues();
                contentValues.put("day", date);
                contentValues.put("data", item.data);
                db.insert(RADIO_TABLE, null, contentValues);
            }
        }
    }

    public TimeItem[] getTimes() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery("SELECT title, body FROM " + TIMES_TABLE, null);
        c.moveToFirst();
        if(c.getCount() > 0) {
            TimeItem[] result = new TimeItem[c.getCount()];
            int i = 0;
            for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
                TimeItem item = new TimeItem();
                item.title = c.getString(0);
                item.body = c.getString(1);
                result[i++] = item;
            }
            c.close();
            return result;
        }
        return null;
    }

    public void insertTimes(TimeItem[] items) {
        SQLiteDatabase db = this.getWritableDatabase();
        if(items != null){
            db.execSQL("drop table IF EXISTS " + TIMES_TABLE );
            db.execSQL("create table IF NOT EXISTS " + TIMES_TABLE + " (id integer primary key, title text, body text)");
            for(TimeItem item : items) {
                ContentValues contentValues = new ContentValues();
                contentValues.put("title", item.title);
                contentValues.put("body", item.body);
                db.insert(TIMES_TABLE, null, contentValues);
            }
        }
    }

}
